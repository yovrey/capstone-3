import './App.css';
import { useState } from "react"

import {UserProvider} from "./UserContext"

// Import routes
import AppNavbar from "./components/AppNavbar"
import ProductView from "./components/ProductView"
import OrderView from "./components/OrderView"
import CreateProduct from "./components/ProductCreate"
import ProductUpdate from "./components/ProductUpdate"
import UserCard from "./components/UserCard"
import Home from "./pages/Home"
import Products from "./pages/Products"
import Orders from "./pages/Orders"
import Register from "./pages/Register"
import Login from "./pages/Login"
import Logout from "./pages/Logout"
import Error from "./pages/Error"


// import Footer from "./components/Footer"
import { Container } from "react-bootstrap"
import { BrowserRouter as Router, Route, Routes } from "react-router-dom"


function App() {
  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  })

  const unsetUser = () => {
    localStorage.clear()
  }

  return (
    <>
      {/*Provides the user context throughout any component inside of it*/}
      <UserProvider value={{user, setUser, unsetUser}}>
      {/*Initializes that dynamic routing will be involved*/}
        <Router>
          <AppNavbar/>
          <Container className="mainPage">

            <Routes>
              

              {
                (user.isAdmin === true)?
                <>
                <Route path="/" element={<Home/>}/>
                <Route path="/products" element={<Products/>}/>
                <Route path="/products/:productId" element={<ProductView/>}/>
                <Route path="/orders" element={<Orders/>}/>
                <Route path="/logout" element={<Logout/>}/>
                <Route path="/login" element={<Login/>}/>
                <Route path="products/create-product" element={<CreateProduct/>}/>
                <Route path="products/:productId/update" element={<ProductUpdate/>}/>
                <Route path="/users/:id" element={<UserCard/>}/>
                </>
                :
                <>
                <Route path="/" element={<Home/>}/>
                <Route path="/products" element={<Products/>}/>
                <Route path="/products/:productId" element={<ProductView/>}/>
                <Route path="/orders" element={<Orders/>}/>
                <Route path="/orders/:orderId" element={<OrderView/>}/>
                <Route path="/login" element={<Login/>}/>
                <Route path="/register" element={<Register/>}/>
                <Route path="/logout" element={<Logout/>}/>
                <Route path="*" element={<Error/>}/> 

                </>
              }
              
            </Routes>
          </Container>
        </Router>   
      </UserProvider> 
    </>

  );
}

export default App;

