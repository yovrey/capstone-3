import {useState, useEffect, useContext} from 'react'
import {Form, Button, Col, Row, Container, Stack, Card} from 'react-bootstrap'
import UserContext from '../UserContext'
import {Navigate, useNavigate} from 'react-router-dom'
import Swal from 'sweetalert2'

export default function CreateProduct(){
	const {user} = useContext(UserContext)

	const navigate = useNavigate()

	const [name, setName] = useState("")
	const [description, setDescription] = useState("")
	const [price, setPrice] = useState("")

	const [isActive, setIsActive] = useState(false)

	function NewProduct(event){
		event.preventDefault()

		fetch(`${process.env.REACT_APP_API_URL}/products/create-product`, {
					method: 'POST',
					headers: {
						'Content-Type': 'application/json'
					},
					body: JSON.stringify({
						name: name,
						description: description,
						price: price						
					})
				})
				.then(response => response.json())
				.then(result => {

					console.log(result)
						// Clears input fields
						setName('')
						setDescription('')
						setPrice('')

						Swal.fire({
							title: 'Saved!',
							icon: 'success',
							text: 'New product created successfully.'
						})

						navigate('/products')
					 
				})
	}

	useEffect(() => {
		if((name !== "" && description !== "" && price !== "") ){
			// Enables the submit button if the form data has been verified
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [name, description, price])

	return(
		(user.isAdmin === false) ?
			<Navigate to="/products"/>
		:
			<Container>
				<Row>
					<Col md={{ span: 6, offset: 3 }} className="p-5">
					<h2 className='text-center'>Product Details</h2>
					<Card className="p-3">
							<Form onSubmit={event => NewProduct(event)}>
								<Row>
										<Form.Group controlId="name">
								            <Form.Label>Product Name</Form.Label>
								            <Form.Control 
								                type="text" 
								                placeholder="Name"
								                value={name}
								                onChange={event => setName(event.target.value)}
								                required
								            />
								        </Form.Group>
									
										<Form.Group controlId="description">
								            <Form.Label>Description</Form.Label>
								            <Form.Control
								            	as="textarea" 
								            	rows={4}
								                type="text" 
								                placeholder="Enter the product description"
								                value={description}
								                onChange={event => setDescription(event.target.value)}
								                required
								            />
								        </Form.Group>
									
								</Row>

						        <Form.Group controlId="price">
						            <Form.Label>Price</Form.Label>
						            <Form.Control 
						                type="number" 
						                placeholder="0.00"
						                value={price}
						                onChange={event => setPrice(event.target.value)}
						                required
						            />
						        </Form.Group>


						        {	isActive ?
						        	<Stack>
						        		<Button className="mt-3" variant="primary" type="submit" id="submitBtn">
							        	Submit
							        	</Button>
						        	</Stack>
						        	
							    :
							        <Stack>
							        	<Button className="mt-3" variant="primary" type="submit" id="submitBtn" disabled>
							        	Submit
							        	</Button>
							        </Stack>
							        
						        }
						        
						    </Form>
					</Card>
							
					</Col>
								
				</Row>
			</Container>
			
	)
}
