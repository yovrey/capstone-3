import { Navbar, Container, Col } from "react-bootstrap"
import Nav from "react-bootstrap/Nav"
import { Link, NavLink } from "react-router-dom"
import { useContext, useEffect, useState} from "react"
import UserContext from "../UserContext"

export default function AppNavbar(){

	const {user} = useContext(UserContext)
	const [count, setCount] = useState("")



	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/orders/user-orders`, {
			headers: {
	        Authorization: `Bearer ${ localStorage.token }`
	    }
		})
		.then(response => response.json())
		.then(result => {

			setCount(result.length)
			console.log(count)
		})	
	}, [count])

	return (
			<>
			<Navbar sticky="top" bg="dark" variant="dark" expand="lg" >
				<Container>
				{ (user.id) ?
					<>
					<Navbar.Brand className="userProfile" as={Link} to="/">{user.firstName} {user.lastName} <h5>O'Zam</h5></Navbar.Brand>
					</>
				:
					<Navbar.Brand className="brand" as={Link} to="/">O'Zam</Navbar.Brand>
				}
					
						<Navbar.Toggle />		
						<Navbar.Collapse id="basic-navbar-nav" className="justify-content-end">
						<Nav>
							<Nav.Link as={NavLink} to="/">Home</Nav.Link>
							<Nav.Link as={NavLink} to="/products">Products</Nav.Link>
							{(user.id) ?
								<>	
									{ (user.isAdmin)?
										<Nav.Link as={NavLink} to="/orders">Orders</Nav.Link>
										:
										<>
										<Nav.Link as={NavLink} to="/orders">My Cart 
{/*											<Button className="disabled" variant=""secondary>{count}</Button>
*/}										</Nav.Link>
										{/*<Nav.Link as={NavLink} to="/orders/user-orders">Orders</Nav.Link>*/}
										</>
									}						
									<Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>					
								</>
								:
								<>
									<Nav.Link as={NavLink} to="/register">Register</Nav.Link>
									<Nav.Link as={NavLink} to="/login">Login</Nav.Link>
								</>
							}
						</Nav>
						</Navbar.Collapse>
				</Container>
			</Navbar>
			<Navbar fixed="bottom" bg="dark" variant="dark" expand="lg">
				<Container className="text-center">
					<Col> </Col>
					<Col>
						<Navbar.Brand className="p-2" as={Link} to="/">O</Navbar.Brand>
					</Col>
					<Col>
						<Navbar.Brand className="p-2" as={Link} to="/">Z</Navbar.Brand>
					</Col>
					<Col>
						<Navbar.Brand className="p-2" as={Link} to="/">A</Navbar.Brand>
					</Col>
					<Col>
						<Navbar.Brand className="p-2" as={Link} to="/">M</Navbar.Brand>
					</Col>
					<Col> </Col>
				</Container>
					
			</Navbar>
			</>			
				
		
		
	)
}