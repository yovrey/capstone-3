import { useState, useEffect } from 'react';
import { Card } from 'react-bootstrap';
import { useParams } from "react-router-dom"



export default function UserProfile() {

	const [firstName, setFirstName] = useState("")
	const [lastName, setLastName] = useState("")
	const [mobileNo, setMobileNo] = useState("")
	const [email, setEmail] = useState("")
	const [orders, setOrders] = useState("")


	const {id} = useParams()

	useEffect((Orders) => {
		fetch(`${process.env.REACT_APP_API_URL}/users/${id}`, {
		})
		.then(response => response.json())
		.then(result => {

			setFirstName(result.firstName)
			setLastName(result.lastName)
			setMobileNo(result.mobileNo)
			setEmail(result.email)
			setOrders((result.orders).map((order) => 
				<li key={order._id}> {order.quantity} - {order.productName} </li> 
			))	
		})
	}, [id])

	return(
		<> 
		<div className="col-lg-6 container-fluid p-3 text-center"> 
		    <Card className="cardProduct p-1" border="info" >
		      <Card.Body>
		        <Card.Title className="p-3">{firstName} {lastName}</Card.Title>
		        <Card.Text>Mobile Number: {mobileNo}</Card.Text>
		        <Card.Text>Email Address: {email}</Card.Text>
		        <Card.Text>Orders:{orders}</Card.Text>
		      </Card.Body>
		    </Card>    
		</div>
		</>
	)
}