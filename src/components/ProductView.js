import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col, Stack } from 'react-bootstrap';
import { useParams, useNavigate, Link } from "react-router-dom"
import UserContext from "../UserContext"
import img from "../images/background.jpg"
import Swal from "sweetalert2"

export default function ProductView() {

	const {productId} = useParams()
	const [name, setName] = useState("")
	const [description, setDescription] = useState("")
	const [price, setPrice] = useState("")
	const [isActive, setIsActive] = useState("")
	const [quantity, setQuantity] = useState(0)
	const [isActiveButton, setIsActiveButton] = useState("")

	const {user} = useContext(UserContext)
	const navigate = useNavigate()


	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/product-details`)
		.then(response => response.json())
		.then(result => {
			console.log(result)
			setName(result.name)
			setDescription(result.description)
			setPrice(result.price)
			setIsActive(result.isActive)

		})
	}, [productId])



// INCREMENT/DECREMENT QUANTITY-------------------------------------
	function increment(){ 
	      setQuantity(quantity + 1)   
	  }
	function decrement(){
		if(quantity !== 0) {
			setQuantity(quantity - 1) 
		}     
	}
// INCREMENT/DECREMENT QUANTITY END---------------------------------



// ADD TO CART--------------------------------------------
	function checkout() {		
		fetch(`${process.env.REACT_APP_API_URL}/orders/checkout`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				userId: user.id,
				productId: productId,
				quantity: quantity
				
			})
		})
		.then(response => response.json())
		.then(result => {
			console.log(result)			
				Swal.fire({
					title: "O'Zam!",
					icon: "success",
					text: "Product was added to your cart."
				})
				navigate("/products")		
		})
	}
// ADD TO CART----------------------------------------



// ARCHIVE A PRODUCT---------------------------------------
	function archive(){
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/archive`, {method: 'PATCH' })
		.then(response => response.json())
		.then(result => {
			setIsActive(false)
			Swal.fire({
				title: 'Success!',
				icon: 'success',
				text: 'Product has been archived.'
			})					
			navigate(`/products/${productId}`)					 
		})
	}
// ARCHIVE A PRODUCT END-----------------------------------



// RESTORE A PRODUCT---------------------------------------
	function restore(){
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/restore`, {method: 'PATCH' })
		.then(response => response.json())
		.then(result => {
			setIsActive(true)
			Swal.fire({
				title: 'Success!',
				icon: 'success',
				text: 'Product has been restored.'
			})
									
			navigate(`/products/${productId}`)					 
		})
	}
// RESTORE A PRODUCT END-----------------------------------



	useEffect(() => {
		if(quantity !== 0) {
			setIsActiveButton(true)
		} else {
			setIsActiveButton(false)
		}
	}, [quantity])


	return(
		<Container className="mt-5">
			<Row>
				<Col className="p-5" lg={{ span: 6, offset: 3 }} >
					<Card border="info" className="p-3">
						<Card.Body className="text-center">
						<Card.Img src={img}/>
							<Card.Header><h3>{name}</h3></Card.Header>
							<Card.Subtitle className="pt-3">Description:</Card.Subtitle>
							<Card.Text>{description}</Card.Text>
							<Card.Text><h3>₱ {price}</h3></Card.Text>
							{ (user.id !== null) ? 
								 (user.isAdmin === true) ?
									 <>
									 <Col className="mb-3">
									 	<Link className="btn btn-primary" to={`/products/${productId}/update`}>Update product details</Link>
									 </Col>

									{(isActive === true) ?
									 	<Col>
									 	<Button variant="danger" onClick={archive}>Archive Product</Button>
									 	</Col>
									 	:
									 	<Col>
									 	<Button variant="success" onClick={restore}>Restore Product</Button>
									 	</Col>
									}
									 </>										
									:
									 <>
									 <Row className="p-3" gap={3}>															
										<Button variant="light">
											<Card.Subtitle><h6>Quantity: {quantity}</h6></Card.Subtitle></Button>	
										<Col>
											<Button variant="secondary" onClick={decrement}>-</Button>
										</Col>
										<Col>												
											<Button variant="secondary" onClick={increment}>+</Button>
										</Col>
									 </Row>
									 { (isActiveButton) ?
										<>
										<Card.Subtitle>Subtotal:</Card.Subtitle>
										<Card.Text>PhP {price*quantity}</Card.Text>
										<Stack>
											<Button variant="primary" onClick={checkout} block>Add to Cart</Button>
										</Stack>
										</>
										:
										<Stack>
											<Button variant="light" onClick={checkout} block disabled></Button>
										</Stack>											
									 }										
									 </>
							:
								<Link className="btn btn-danger btn-block" to="/login">Log in to order</Link>
							}

							
						</Card.Body>		
					</Card>
				</Col>
			</Row>
		</Container>
	)
}