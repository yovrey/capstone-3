import {Card} from 'react-bootstrap';



export default function UserOrdersCard({order}) {

  // Destructuring the props
  const {productName, quantity, totalAmount, _id} = order

  return (
    <> 
    <div className="col-lg-4 container-fluid p-3 text-center"> 
        <Card className="cardProduct p-1" border="info" >
          <Card.Body>
            <Card.Title className="p-3">{productName}</Card.Title>
            <Card.Subtitle className="pt-1">{quantity}</Card.Subtitle>
            <Card.Text>{totalAmount}</Card.Text>
          </Card.Body>
        </Card>    
    </div>
    </>



  )
}