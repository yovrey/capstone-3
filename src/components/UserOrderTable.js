import { useContext } from "react"
import {Table, Col, Row, Container, Button} from 'react-bootstrap'
import { useNavigate } from "react-router-dom"
import UserContext from '../UserContext'
import { Link } from "react-router-dom"
import Swal from "sweetalert2"




export default function OrderTable({order}) {
	
	const {user} = useContext(UserContext)
	const navigate = useNavigate()
	const [isLoading, setIsLoading] = useState(false)

	// Destructure props
   const {userId, totalAmount, purchasedOn, productName, quantity, _id} = order


	useEffect((isLoading) => {
		fetch(`${process.env.REACT_APP_API_URL}/orders/user-cart`, {
			headers: {
	        Authorization: `Bearer ${ localStorage.token }`
	    }
		})
		.then(response => response.json())
		.then(result => {
			setTotalAmount(result.reduce((n, {totalAmount}) => n + totalAmount, 0))
			setOrders(
				result.map((order, id) => {
					return <li key={id} order={order}/>
				})
			)
			setIsLoading(false)
		})	
	}, [user])




  return (
    <Table striped bordered hover>
		      <tbody>
		        <tr>
			        <Container>
					     	<Row className="text-center" md={5}>					     		
					     		<Col>
					     			<Button onClick={Order}>Place Order</Button>
					     		</Col>
					     		<Col>
					     			{productName}
					     		</Col>	
					     		
					     		<Col>
					     			{quantity}
					     		</Col>
					     		<Col>
					     			{totalAmount}
					     		</Col>
					     		<Col>
					     			<Link className="btn btn-warning mt-2" to={`/orders/${_id}`}>Customize Order</Link>
					     		</Col>
					     	</Row>
					     </Container>   
		        </tr>
		      </tbody>
		  </Table>
  );
}