import {Row, Col, Card, Container} from "react-bootstrap"
import img1 from "../images/image-sample.jpg"

export default function Highlights(){
	return (
		<Container>
			<Row className="mt-3 mb-3 justify-text-center" bg="info">
				<Col xs={12} sm={12} md={4} className="p-2">
				    <Card className="cardHighlight p-3 " style={{ width: '20rem' }} border="info">
				    <Card.Img src={img1}/>
				        <Card.Body>
				        	<Card.Header>
				        		<h3>What is Freediving?</h3>
				        	</Card.Header>	   
				            <Card.Text>
				                Freediving or breath-hold diving is a method of underwater diving that does not require the help of a breathing apparatus. Instead of using an air tank, you simply hold your breath for as long can until you return to the surface. With freediving, you can only travel as far as the air in your lungs will take you.
				            </Card.Text>
				        </Card.Body>
				    </Card>
				</Col>
				<Col xs={12} sm={12} md={4} className="p-2">
				    <Card className="cardHighlight p-3" style={{ width: '20rem' }} border="info">
				    <Card.Img src={img1}/>
				        <Card.Body>
				            <Card.Header>
				                <h3>Why Freediving?</h3>
				            </Card.Header>
				            <Card.Text>
				                With relaxed breathing like you will learn in a freediving course you can use it to reduce stress and oxygenate your body with very little effort. It can help in yoga, stressful situations, when you're tired, or even when you're sick and your lungs aren't working as well.
				            </Card.Text>
				        </Card.Body>
				    </Card>
				</Col>
				<Col xs={12} sm={12} md={4} className="p-2">
				    <Card className="cardHighlight p-3" style={{ width: '20rem' }} border="info">
				    <Card.Img src={img1}/>
				        <Card.Body>
				            <Card.Header>
				                <h3>What equipment is needed for freediving?</h3>
				            </Card.Header>
				            <Card.Text>
				                Beginner freedivers should invest, at the very least, in a mask, a snorkel, and a set of bifins. A wetsuit, weight belt, weights, safety lanyard, socks, and gloves may also be needed depending on if you are training and where you are diving.
				            </Card.Text>
				        </Card.Body>
				    </Card>
				</Col>
			</Row>
		</Container>
		
	)
}