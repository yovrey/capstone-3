import {Button, Row, Col, Container} from "react-bootstrap"
import { Link } from "react-router-dom"

export default function Banner(){
	return (
		<Container>
			<Row>
				<Col className="p-5" md={{ span: 11, offset: 1 }} >
					<h1>O'Zam Freediving Shop</h1>
					<h6>A freediving shop that offers the best quality freediving equiptments at a lower price!</h6>
					<Link className="btn btn-info" to="/products">Shop now!</Link>
				</Col>
			</Row>			
		</Container>	
		


		
	)
}