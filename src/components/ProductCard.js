import {Card, Container} from 'react-bootstrap';
import { Link } from "react-router-dom"
import img1 from "../images/image-sample.jpg"



export default function ProductCard({product}) {

  // Destructuring the props
  const {name, price, _id} = product

  return (
    <> 
    
    <div className="col-lg-4 container-fluid p-3"> 
      <Container>
        <Card className="cardProduct p-1" border="info" >
          <Card.Body>
          <Card.Img src={img1}/>
            <Card.Title className="p-3">{name}</Card.Title>
            <Card.Text><h3>₱ {price}</h3></Card.Text>
            <div className="text-center">
              <Link className="btn btn-primary mt-2" to={`/products/${_id}`}>Details</Link>
            </div>            
          </Card.Body>
        </Card>  
      </Container>      
    </div>
    
    
    </>



  );
}


