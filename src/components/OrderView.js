import { useState, useEffect } from 'react';
import { Card, Button, Col, Row } from 'react-bootstrap';
import { useParams, useNavigate } from "react-router-dom"
import  img from "../images/image-sample.jpg"
import Swal from "sweetalert2"




export default function OrderView() {

	// Destructuring the props
	const {orderId} = useParams()
	const [productName, setProductName] = useState("");
	const [quantity, setQuantity] = useState("")
	const [purchasedOn, setPurchasedOn] = useState("")
	const [price, setPrice] = useState("")

	const navigate = useNavigate()


  useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/orders/${orderId}`)
		.then(response => response.json())
		.then(result => {
			console.log(result)
			setProductName(result.productName)
			setQuantity(result.quantity)
			setPurchasedOn(result.purchasedOn)
			setPrice(result.totalAmount/result.quantity)


		})
	}, [orderId])


// INCREMENT/DECREMENT-------------------------
	function increment(){
	    setQuantity(quantity + 1)  
	  }
	function decrement(){
		if(quantity !== 1) {
			setQuantity(quantity - 1)
		}     
	}



// DELETE ORDER---------------------------------
  	function Remove(){
		Swal.fire({
		  title: 'Are you sure?',
		  text: "You won't be able to revert this!",
		  icon: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes, delete it!'
		})
		.then((result) => {
			console.log(result)
			if (result.isConfirmed){
				fetch(`${process.env.REACT_APP_API_URL}/orders/${orderId}/delete`, {method: 'DELETE'})
				.then(response => response.json())
				.then(result => {
					Swal.fire({
					title: "Deleted!",
					icon: "success",
					text: "Your file has been deleted."
				})
				navigate("/orders")
				})
			}
		})		
	}	
// DELETE ORDER END-----------------------------


// UPDATE ORDER---------------------------------
function update(){
	fetch(`${process.env.REACT_APP_API_URL}/orders/${orderId}/update-order`, {
		method: 'PATCH',
		headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				quantity: quantity						
			})
	})
		.then(response => response.json())
		.then(result => {
			Swal.fire({
				title: 'Success!',
				icon: 'success',
				text: 'Order has been updated.'
			})

		})
}
// UPDATE ORDER END---------------------------------




  return (
    <> 
    <div className="col-lg-4 container-fluid p-3 text-center"> 
        <Card className="cardProduct p-1" border="info" >
        <Card.Img src={img}/>
          <Card.Body>
            <Card.Title className="p-3">{productName}</Card.Title>
            <Row className="p-3" gap={3}>															
										<Button variant="light">
											<Card.Subtitle><h6>Quantity: {quantity}</h6></Card.Subtitle></Button>	
										<Col>
											<Button variant="secondary" onClick={decrement}>-</Button>
										</Col>
										<Col>												
											<Button variant="secondary" onClick={increment}>+</Button>
										</Col>
						</Row>
            <Card.Subtitle className="pt-1">Subtotal:</Card.Subtitle>
            <Card.Text>PhP {price*quantity}</Card.Text>
            <Card.Subtitle className="pt-1">Added On:</Card.Subtitle>
            <Card.Text>{purchasedOn}</Card.Text>
        	<Col>
        		<Button className="m-2" variant="danger" onClick={Remove}>Delete Item</Button>
        		<Button variant="primary" onClick={update}>Save Changes</Button>
        	</Col>
          </Card.Body>
        </Card>    
    </div>
    </>



  );
}
