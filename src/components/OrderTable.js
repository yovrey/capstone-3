import { useContext } from "react"
import {Table, Col, Row, Container, Button} from 'react-bootstrap'
import { useNavigate } from "react-router-dom"
import UserContext from '../UserContext'
import { Link } from "react-router-dom"
import Swal from "sweetalert2"




export default function OrderTable({order}) {
	
	const {user} = useContext(UserContext)
	const navigate = useNavigate()

	// Destructure props
  const {userId, totalAmount, purchasedOn, productName, quantity, _id} = order

	// Place Order---------------------------------------
	function Order(event){
		Swal.fire({
		  title: 'Are you sure?',
		  text: "You won't be able to revert this!",
		  icon: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes, place order!'
		}).then((result) => {
				if (result.isConfirmed) {
					fetch(`${process.env.REACT_APP_API_URL}/orders/${_id}/add-order`, {method: 'PATCH' })
					.then(response => response.json())
					.then(result => {
						Swal.fire({
							title: 'Success!',
							icon: 'success',
							text: 'Your order will be dispatched within 24 hours.'
						})									
						navigate(`/products`)					 
					})
				}
		})		
	}
// Place Order END-----------------------------------


  return (
  	(user.isAdmin) ?
    
	      <Table striped bordered hover>
		      <tbody>
		        <tr>
			        <Container>
					     	<Row className="text-center" md={6}>
					     		<Col>
					     			{_id}
					     		</Col>
					     		<Col>
					     			<Link to={`/users/${userId}`}>{userId}</Link>
					     		</Col>
					     		<Col>
					     			{productName}
					     		</Col>
					     		<Col>
					     			{quantity}
					     		</Col>
					     		<Col>
					     			{totalAmount}
					     		</Col>
					     		<Col>
					     			{purchasedOn}
					     		</Col>
					     	</Row>
					     </Container>   
		        </tr>
		      </tbody>
		  </Table>
	    
    :
    <Table striped bordered hover>
		      <tbody>
		        <tr>
			        <Container>
					     	<Row className="text-center" md={5}>					     		
					     		<Col>
					     			<Button onClick={Order}>Place Order</Button>
					     		</Col>
					     		<Col>
					     				{productName}
					     		</Col>	
					     		
					     		<Col>
					     			{quantity}
					     		</Col>
					     		<Col>
					     			{totalAmount}
					     		</Col>
					     		<Col>
					     			<Link className="btn btn-warning mt-2" to={`/orders/${_id}`}>Customize Order</Link>
					     		</Col>
					     	</Row>
					     </Container>   
		        </tr>
		      </tbody>
		  </Table>
  );
}