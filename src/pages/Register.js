import {useState, useEffect, useContext} from 'react'
import {Form, Button, Col, Row, Container, Stack, Card} from 'react-bootstrap'
import UserContext from '../UserContext'
import {Navigate, useNavigate} from 'react-router-dom'
import Swal from 'sweetalert2'

export default function Register(){
	const {user} = useContext(UserContext)

	const navigate = useNavigate()

	const [firstName, setFirstName] = useState("")
	const [lastName, setLastName] = useState("")
	const [mobileNo, setMobileNo] = useState("")
	const [email, setEmail] = useState("")
	const [password1, setPassword1] = useState("")
	const [password2, setPassword2] = useState("")


	// For determining if button is disabled or not
	const [isActive, setIsActive] = useState(false)

	function registerUser(event){
		event.preventDefault()

		fetch(`${process.env.REACT_APP_API_URL}/users/check-email`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(response => response.json())
		.then(result => {
			if(result === true){
				Swal.fire({
					title: 'Oops!',
					icon: 'error',
					text: 'Email is already in use!'
				})
			} else {
				fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
					method: 'POST',
					headers: {
						'Content-Type': 'application/json'
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						mobileNo: mobileNo,
						email: email,
						password: password1
					})
				})
				.then(response => response.json())
				.then(result => {
					if(result !== false){
						// Clears out the input fields after form submission
						setFirstName('')
						setLastName('')
						setMobileNo('')
						setEmail('')
						setPassword1('')
						setPassword2('')


						Swal.fire({
							title: 'Success!',
							icon: 'success',
							text: 'You have successfully registered!'
						})

						navigate('/login')
					} else {
						Swal.fire({
							title: 'Oops!',
							icon: 'error',
							text: "Something went wrong :("
						})
					}
				})
			}
		})
	}

	useEffect(() => {
		if((firstName !== "" && lastName !== "" && mobileNo !== "" && email !== "" && password1 !== "" && password2 !== "") && (password1 === password2)){
			// Enables the submit button if the form data has been verified
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [firstName, lastName, mobileNo, email, password1, password2])

	return(
		(user.id !== null) ?
			<Navigate to="/products"/>
		:
			<Container className="mt-5">
				<Row>
					<Col md={{ span: 6, offset: 3 }} className="p-5">
					<Card className="p-3">
						<Card.Header as="h2" className="text-center">Register</Card.Header>
							<Form onSubmit={event => registerUser(event)}>
								<Row className="mt-3">
									<Col>
										<Form.Group controlId="firstName">
								            <Form.Label>First Name</Form.Label>
								            <Form.Control 
								                type="text" 
								                placeholder="Enter your first name"
								                value={firstName}
								                onChange={event => setFirstName(event.target.value)}
								                required
								            />
								        </Form.Group>
									</Col>
									<Col>
										<Form.Group controlId="lastName">
								            <Form.Label>Last Name</Form.Label>
								            <Form.Control 
								                type="text" 
								                placeholder="Enter your last name"
								                value={lastName}
								                onChange={event => setLastName(event.target.value)}
								                required
								            />
								        </Form.Group>
									</Col>
								</Row>

						        <Form.Group controlId="mobileNo">
						            <Form.Label>Mobile Number</Form.Label>
						            <Form.Control 
						                type="text" 
						                placeholder="Enter your mobile number"
						                value={mobileNo}
						                onChange={event => setMobileNo(event.target.value)}
						                required
						            />
						        </Form.Group>

						        <Form.Group controlId="userEmail">
						            <Form.Label>Email address</Form.Label>
						            <Form.Control 
						                type="email" 
						                placeholder="Enter email"
						                value={email}
						                onChange={event => setEmail(event.target.value)}
						                required
						            />
						            <Form.Text className="text-muted">
						                We'll never share your email with anyone else.
						            </Form.Text>
						        </Form.Group>

						        <Form.Group controlId="password1">
						            <Form.Label>Password</Form.Label>
						            <Form.Control 
						                type="password" 
						                placeholder="Password"
						                value={password1}
						                onChange={event => setPassword1(event.target.value)}
						                required
						            />
						        </Form.Group>

						        <Form.Group controlId="password2">
						            <Form.Label>Verify Password</Form.Label>
						            <Form.Control 
						                type="password" 
						                placeholder="Verify Password" 
						                value={password2}
						                onChange={event => setPassword2(event.target.value)}
						                required
						            />
						        </Form.Group>						        

						        {	isActive ?
						        	<Stack>
						        		<Button className="mt-3" variant="primary" type="submit" id="submitBtn">
							        	Submit
							        	</Button>
						        	</Stack>	
							    	:
							        <Stack>
							        	<Button className="mt-3" variant="primary" type="submit" id="submitBtn" disabled>
							        	Submit
							        	</Button>
							        </Stack>							        
						        }						        
						    </Form>
					</Card>							
					</Col>								
				</Row>
			</Container>
			
	)
}
