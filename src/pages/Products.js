import ProductCard from "../components/ProductCard"
import Loading from "../components/Loading"
import { useEffect, useState, useContext } from "react"
import { Link } from "react-router-dom"
import { Stack, Row } from "react-bootstrap"
import UserContext from "../UserContext"

export default function Products(){

	const {user} = useContext(UserContext)
	const [products, setProducts] = useState([])
	const [isLoading, setIsLoading] = useState(false)

	useEffect((Products) => {
		// Sets the loading state to true
		setIsLoading(true)
		if(user.isAdmin){
            fetch(`${process.env.REACT_APP_API_URL}/products/`)
            .then(response => response.json())
            .then(result => {
            	console.log(result)
                setProducts(   
                    result.map((product, id) => {
                        return (
                        	<ProductCard key={id} product={product}/>
						)                    
                    })                   
                )
                console.log(products)
            // Sets the loading state to false
            setIsLoading(false)
            })
        }

		fetch(`${process.env.REACT_APP_API_URL}/products/active`)
		.then(response => response.json())
		.then(result => {
			setProducts(result.map((product, id) => {
					return (						
							<ProductCard key={id} product={product}/>		
					)
				})
			)
			// Sets the loading state to false
			setIsLoading(false)
		})
	}, [user])

	return (			 
		(isLoading) ?
		 		<Loading/>
		 	:
		 		(user.isAdmin === true) ?
		 				<>
		 				<Stack direction="horizontal" gap={5}>
		 					<div>
		 						<h3 className="mt-4">Our Products</h3>
		 					</div>
		 					<div className="vr mt-4" />
		 					<div>
		 						<Link className="mt-4 btn btn-info ms-auto" to="/products/create-product"><h6 className="mt-1">Add a New Product</h6></Link>
		 					</div>	
		 				</Stack>
		 				<Row>
		 					{products}
		 				</Row>	 				
		 				</>
		 				:
		 				<>
		 				<h3 className="mt-3">Our Products</h3>
		 				<Row className="mb-5">
		 					{products}
		 				</Row>
		 				
		 				</>			
	)
}
