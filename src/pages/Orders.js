import OrderTable from "../components/OrderTable"
import Loading from "../components/Loading"
import {Table, Container, Row, Col, } from "react-bootstrap"
import { useEffect, useState, useContext } from "react"
import { Navigate } from 'react-router-dom'
import UserContext from "../UserContext"



export default function Orders(){

	
	const {user} = useContext(UserContext)

	const [orders, setOrders] = useState("")
	const [totalAmount, setTotalAmount] = useState("")
	const [isLoading, setIsLoading] = useState(false)


	useEffect((isLoading) => {
		// Sets the loading state to true
		setIsLoading(true)
		if(user.isAdmin){
			fetch(`${process.env.REACT_APP_API_URL}/orders/`)
			.then(response => response.json())
			.then(result => {
				setOrders(
					result.map((order, id) => {
						return (						
								<OrderTable key={id} order={order}/>		
						)
					})
				)
				setIsLoading(false)
			})
		}
		else {
			fetch(`${process.env.REACT_APP_API_URL}/orders/user-cart`, {
				headers: {
		        Authorization: `Bearer ${ localStorage.token }`
		    }
			})
			.then(response => response.json())
			.then(result => {
				setTotalAmount(result.reduce((n, {totalAmount}) => n + totalAmount, 0))
				setOrders(
					result.map((order, id) => {
						return (<OrderTable key={id} order={order}/>)
					})
				)
				setIsLoading(false)
			})
		}	
	}, [user])






	return (	
	(user.id == null) ?
			<Navigate to="*"/>	
		: 

		(isLoading) ?
		 		<Loading/>
		 	:
		 		(user.isAdmin) ?
		 		<>
		 		<h3 className="mt-3">All Orders</h3>
		 		<Table className="orderTable mt-5" striped bordered hover variant="info">
				   <thead>
				     <tr>
				     <Container>
				     	<Row className="text-center">
				     		<Col>
				     			<h5>Order ID</h5>
				     		</Col>
				     		<Col>
				     			<h5>User ID</h5>
				     		</Col>
				     		<Col>
				     			<h5>Product Name</h5>
				     		</Col>
				     		<Col>
				     			<h5>Quantity</h5>
				     		</Col>
				     		<Col>
				     			<h5>Subtotal</h5>
				     		</Col>
				     		<Col>
				     			<h5>Purchase Date</h5>
				     		</Col>
				     	</Row>
				     </Container>
				       
				     </tr>
				   </thead>
			      <tbody>
			      </tbody>
			    </Table>
		 		{orders}
		 		</>
		 		:
		 		<>
		 		<h3 className="mt-3">My Cart</h3>
		 		<Table className="orderTable mt-5" striped bordered hover variant="info">
				   <thead>
				     <tr>
				     <Container>
				     	<Row className="text-center" md={5}>
					     	<Col>
					     		<h5>Product</h5>
					     	</Col>
				     		<Col>
				     			<h5>Product Name</h5>
				     		</Col>
				     		<Col>
				     			<h5>Quantity</h5>
				     		</Col>
				     		<Col>
				     			<h5>Subtotal</h5>
				     		</Col>
				     		<Col>
				     			<h5>Customize Order</h5>
				     		</Col>
				     	</Row>
				     </Container>				       
				     </tr>
				   </thead>
			      <tbody>
			      </tbody>
			    </Table>
		 		{orders}
		 		<h2 className="pt-5">
		 		   	Total: ₱{totalAmount}
		 		</h2>
		 		</>			
	)
	
} 