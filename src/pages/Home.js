import Banner from "../components/Banner"
import Highlights from "../components/Highlights"
import {Row, Col, Container} from "react-bootstrap"


export default function Home(){
	return (
		<>
		<div className="homePage">
			<Container>
				<Row>			
					<Banner/>			
				</Row>
				<Row>
					<Highlights/>
				</Row>
			</Container>
		</div>
		</>
		
	)
}