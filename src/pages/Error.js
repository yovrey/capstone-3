import {Link} from "react-router-dom"


export default function Error() {
	return (
		<>
			<h1 className="pt-5">This site can’t be reached</h1>
			<p>Please check if the spellling is correct.</p>
			<Link to="/">Go back to home</Link>
		</>
	)
}