import { useState, useEffect, useContext } from "react"
import { Form, Button, Stack, Container, Row, Col, Card } from "react-bootstrap"
import { useNavigate, Navigate } from "react-router-dom"
import UserContext from "../UserContext"
import Swal from "sweetalert2"

export default function Login() {
	// Initializes the use of the properties from the UserProvider in App.js file
	const {user, setUser, unsetUser} = useContext(UserContext)
	// State hooks to store the values of the input fields
	const [email, setEmail] = useState("")
	const [password, setPassword] = useState("")

	const navigate = useNavigate()

	// For determining if the button is disabled or not
	const [isActive, setIsActive] = useState(false)
	const[isAdmin, setIsAdmin]=useState(false);



	const retrieveUser = (token) => {
		fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		.then(response => response.json())
		.then(result => {
			console.log(result)

			// Store the user details retrieved from the token into the global user state
			setUser({
				id: result._id,
				firstName: result.firstName,
				lastName: result.lastName,				
				isAdmin: result.isAdmin,
				orders: result.orders
			})
		})
	}




	function authenticate(event) {
		event.preventDefault()

		fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(response => response.json())
		.then(result => {
			console.log(result)
			if(typeof result.accessToken !== "undefined"){
				localStorage.setItem("token", result.accessToken)

				retrieveUser(result.accessToken)

				Swal.fire({
					title: `Awesome!`,
					icon: "success",
					text: "Welcome to our shop!"
				})	
			}
			else {
					Swal.fire({
					title: "Authentication Failed!",
					icon: "error",
					text: "Please try again."
				})
			}
		})
	}


	useEffect(() => {
		if(email !== "" && password !== "") {
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [email, password])

	return (
		(user.id !== null) ?
				<Navigate to="/products"/>
			:
				<Container className="mt-5">
					<Row>
						<Col md={{ span: 6, offset: 3 }} className="p-5">							
						<Card className="p-3">
						<Card.Header as="h2" className="text-center">Login</Card.Header>
							<Form onSubmit={event => authenticate(event)}>
								<Form.Group controlId="userEmail" className="mt-3">
						            <Form.Label>Email address</Form.Label>
						            <Form.Control 
						                type="email" 
						                placeholder="Enter email"
						                value={email}
						                onChange={event => setEmail(event.target.value)}
						                required
						            />
						            <Form.Text className="text-muted">
						                We'll never share your email with anyone else.
						            </Form.Text>
						        </Form.Group>

						        <Form.Group controlId="password">
						            <Form.Label>Password</Form.Label>
						            <Form.Control 
						                type="password" 
						                placeholder="Password"
						                value={password}
						                onChange={event => setPassword(event.target.value)} 
						                required
						            />
						        </Form.Group>


						        {	isActive ? 
						        	<Stack>
						        		<Button className="mt-3" variant="primary" type="submit" id="submitBtn">
						        		Submit
						        		</Button>
						        	</Stack>
						        	
						        :
						        	<Stack>
						        		<Button className="mt-3" variant="primary" type="submit" id="submitBtn" disabled>
						        		Submit
						        		</Button>
						        	</Stack>						        	
						        }		            
							</Form>
						</Card>					
														
						</Col>
					</Row>
				</Container>
				
					
	)
}